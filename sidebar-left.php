<?php if (is_active_sidebar('sidebar-left')) { ?>
				<div class="col-xs-6  col-sm-4 col-md-3">
                    <div class="topfil hidden-xs">
                    <img src="<?php echo theme_root(); ?>/img/filrouge-top.png" alt="fil rouge top" />
                    </div>
					<nav class="navbar navbar-left">
                    <!--navbar-collapse-->
                    <div class="collapse navbar-collapse navbar-primary-collapse hidden-xs menu-gauche">
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu' => 'Menu 1', 'container' => false, 'container_id' => FALSE, 'menu_class' => 'nav navbar-nav', 'menu_id' => FALSE, 'depth' => 1, 'walker' => new Description_Walker)); ?>
					</div>
					</nav><!--navbar-collapse-->
                    <div class="topfil-b hidden-xs">
                    <img src="<?php echo theme_root(); ?>/img/filrouge-b.png" alt="fil rouge b" />
                    </div>
                    <div class="texte-menu hidden-xs">
                    Les Petits Chaperons Rouges conçoivent, réalisent, animent <br />et gèrent des crèches…
                    <br />
                    <a href="http://www.lpcr.fr" target="_blank">En savoir +</a>
                    </div>
				</div>
<?php } ?>
