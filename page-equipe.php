<?php
/**
/* Template Name: Equipe */
/* Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
get_header();
?> 
<?php get_sidebar('left'); ?> 
				<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
					<main id="main" class="site-main">
                    <div class="content-top"></div>
                    <!-- Votre crèche -->
                        <div class="cadre-post col-xs-12 col-sm-12 col-md-12 cadre-eq"><div class="profil-post">
 							<div class="post-title pull-left"><h2>Votre crèche</h2></div><div class="post-title2 pull-left">Les Petits Chaperons Rouges</div><div class="post-title2r">Montrouge</div>
                            <br />
							<div class="excerpt">
							
                            <div class="equipe col-xs-12 col-sm-12 col-md-6" >
                            <div class="profil-p2">
                            <svg width="120" height="110" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask5" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m5" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m5-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/1.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask5)" id="p5" y="0%" x="0%" width="100%" height="100%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/1@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask5)" id="p5-2x" y="0%" x="0%" width="100%" height="100%" />

                            </svg>
                            </div>
							<div class="nomprofil3">Pierre Dupont</div>
                            <div class="statut-profil">Directeur</div>
							<div class="description-profil"></div>
                            </div>
                            
                            <div class="equipe col-xs-12 col-sm-12 col-md-6" >
                            <div class="profil-p2">
                            <svg width="120" height="110" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask6" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m6" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m6-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/2.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask6)" id="p6" y="0%" x="0%" width="100%" height="100%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/2@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask6)" id="p6-2x" y="0%" x="0%" width="100%" height="100%" />

                            </svg>
                            </div>
							<div class="nomprofil3">Jeanine Jean</div>
                            <div class="statut-profil">Adjointe</div>
							<div class="description-profil"></div>
                            </div>
                            
                            <div class="equipe col-xs-12 col-sm-12 col-md-6" >
                            <div class="profil-p2">
                            <svg width="120" height="110" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask7" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m7" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m7-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/3.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask7)" id="p7" y="0%" x="0%" width="100%" height="100%" />									
									<image xlink:href="<?php echo theme_root()."/img/profils/3@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask7)" id="p7-2x" y="0%" x="0%" width="100%" height="100%" />

                            </svg>
                            </div>
							<div class="nomprofil3-avecdesc">Dr Georges Pillule</div>
                            <div class="statut-profil">Médecin</div>
							<div class="description-profil">Présent du lundi au vendredi</div>
                            </div>

							</div>
                            <div class="clearfix"></div><br /><br />
						</div></div>
                        <!-- /Votre crèche -->

                        
                        <!-- Section grands -->
                        <div class="cadre-post col-xs-12 col-sm-12 col-md-12 cadre-eq2"><div class="profil-post">
 							<div class="post-title pull-left"><h2>Section grands</h2></div>
                            <br />
							<div class="excerpt">
							
                            <div class="equipe col-xs-12 col-sm-12 col-md-6" >
                            <div class="profil-p2">
                            <svg width="120" height="110" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask8" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m8" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m8-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/4.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask8)" id="p8" y="0%" x="0%" width="100%" height="100%" />									
									<image xlink:href="<?php echo theme_root()."/img/profils/4@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask8)" id="p8-2x" y="0%" x="0%" width="100%" height="100%" />

                            </svg>
                            </div>
							<div class="nomprofil3-avecdesc">Emmanuelle Dubois</div>
                            <div class="statut-profil">EJE</div>
							<div class="description-profil">Loren Ipsum dolor sit amet, consetetur sadipscing elitr, sed diam.</div>
                            </div>
                            
                            <div class="equipe col-xs-12 col-sm-12 col-md-6" >
                            <div class="profil-p2">
                            <svg width="120" height="110" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask9" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m9" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m9-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/5.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask9)" id="p9" y="0%" x="0%" width="100%" height="100%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/5@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask9)" id="p9-2x" y="0%" x="0%" width="100%" height="100%" />

                            </svg>
                            </div>
							<div class="nomprofil3-avecdesc">John Doe</div>
                            <div class="statut-profil">Agent</div>
							<div class="description-profil">Loren Ipsum dolor sit amet, consetetur sadipscing elitr, sed diam.</div>
                            </div>
                            
							</div>
                            <div class="clearfix"></div><br /><br />
						</div></div>
                        <!-- /Section grands -->
            
                        
                        <!-- Section bébés -->
                        <div class="cadre-post col-xs-12 col-sm-12 col-md-12 cadre-eq2"><div class="profil-post">
 							<div class="post-title pull-left"><h2>Section bébés</h2></div>
                            <br />
							<div class="excerpt">
							
                            <div class="equipe col-xs-12 col-sm-12 col-md-6" >
                            <div class="profil-p2">
                            <svg width="120" height="110" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask10" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m10" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m10-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/6.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask10)" id="p10" y="0%" x="0%" width="100%" height="100%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/6@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask10)" id="p10-2x" y="0%" x="0%" width="100%" height="100%" />

                            </svg>
                            </div>
							<div class="nomprofil3">Alice Durand</div>
                            <div class="statut-profil">EJE</div>
							<div class="description-profil"></div>
                            </div>
                            
                            <div class="equipe col-xs-12 col-sm-12 col-md-6" >
                            <div class="profil-p2">
                            <svg width="120" height="110" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask11" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m11" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m11-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/7.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask11)" id="p11" y="0%" x="0%" width="100%" height="100%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/7@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask11)" id="p11-2x" y="0%" x="0%" width="100%" height="100%" />

                            </svg>
                            </div>
							<div class="nomprofil3-avecdesc">Jeanine Jean</div>
                            <div class="statut-profil">Adjointe</div>
							<div class="description-profil">Vient habituellement le mardi après-midi.</div>
                            </div>
                            
							</div>
                            <div class="clearfix"></div><br /><br />
                        </div></div>
						<!-- /Section bébés -->

                        						
					</main>
				</div>
                <div class="clearfix"></div>
                <br />
<?php //get_sidebar('right'); ?> 
			</div>  
             <!--site-content-->
<?php get_footer(); ?> 