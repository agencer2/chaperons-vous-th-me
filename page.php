<?php
/**
 * Template for displaying pages
 * Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
get_header();
?> 
<?php get_sidebar('left'); ?> 
				<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
					<main id="main" class="site-main" role="main">
                    <div class='content-top'></div>
                    <div class='cadre-top-title'>
                    </div>
						<?php if (have_posts()) { ?> 
						<?php 
						// start the loop
						while (have_posts()) {
							echo "<div class='cadre-post col-xs-12 col-sm-12 col-md-12'><div class='profil-post'>"; the_post(); ?>
 							<?php echo "<div class='post-title'><h2><a href='";
							echo the_permalink()."' rel='bookmark' title='Permanent Link to ";
							echo the_title_attribute()."'>";
							echo the_title();
							echo "</a></h2></div>";
							echo "<div class='excerpt'>";
							the_content();
							echo "</div></div></div>";
						}// end while
						customBasicPagination();
						?> 
						<?php } else { ?> 
						<?php get_template_part('no-results', 'index'); ?>
						<?php }// endif; ?> 
					</main>
				</div>
<?php //get_sidebar('right'); ?> 
			</div>  
             <!--site-content-->
<?php get_footer(); ?> 