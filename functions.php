<?php
/**
 *  Les Petits Chaperons Rouges
 * Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */


/**
 * Required WordPress variable.
 */
if (!isset($content_width)) {
	$content_width = 1170;
}
if (!function_exists('customBasicSetup')) {
	/**
	 * Setup theme and register support wp features.
	 */
	function customBasicSetup()
	{
		/**
		 * Make theme available for translation
		 * Translations can be filed in the /languages/ directory
		 *
		 * copy from underscores theme
		 */
		load_theme_textdomain('custom-basic', get_template_directory() . '/languages');

		// add theme support post and comment automatic feed links
		add_theme_support('automatic-feed-links');

		// enable support for post thumbnail or feature image on posts and pages
		add_theme_support('post-thumbnails');

		// add support menu
		register_nav_menus(array(
			'primary' => __('Primary Menu', 'custom-basic'),
		));

		// add post formats support
		add_theme_support('post-formats', array('aside', 'image', 'video', 'quote', 'link'));

		// add support custom background
		add_theme_support(
			'custom-background',
			apply_filters(
				'custom_basic_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => ''
				)
			)
		);
	}// customBasicSetup
}
add_action('after_setup_theme', 'customBasicSetup');
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'profil-size', 70, 50, true );
}
function custom_excerpt_length( $length ) {
	return 62;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function theme_root() {
$theme = get_theme_root_uri()."/".get_template(); return $theme;
}
function custom_wp_trim_excerpt($text) {
$raw_excerpt = $text;
if ( '' == $text ) {
    //Retrieve the post content.
    $text = get_the_content('');

    //Delete all shortcode tags from the content.
    $text = strip_shortcodes( $text );

    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]&gt;', $text);

    $text = strip_tags($text, '<em><br><p>');

    $excerpt_word_count = 130; /*** MODIFY THIS. change the excerpt word count to any integer you like.***/
    $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);
 /**<a href="'. get_permalink($post->ID) . '">--read more</a>*/
    $excerpt_end = '<div class="excerpt-readmore"></div>'; /*** MODIFY THIS. change the excerpt endind to something else.***/
    $excerpt_more = apply_filters('excerpt_more', '' . $excerpt_end);

    $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
    if ( count($words) > $excerpt_length ) {
        array_pop($words);
        $text = implode(' ', $words).$excerpt_more;
        $text = force_balance_tags($text) ;
    } else {
        $text = implode(' ', $words);
    }
}
return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_wp_trim_excerpt');
function get_cat_icon($the_categories) {
echo "<div class='cat-icon'>";
foreach( $the_categories as $category) {
if ($category->cat_name == "Articles") {
$filec = get_theme_root_uri()."/".get_template()."/img/articles.svg";
$filecprint = "<div class='icon-p'><img src='".$filec."' alt='".$category->cat_name."' data-no-retina /></div>";
};
if ($category->cat_name == "Albums") {
$filecprint = '';$filec = '';
$filec = get_theme_root_uri()."/".get_template()."/img/albums.svg";
$filecprint = "<img src='".$filec."' alt='".$category->cat_name."' data-no-retina />";
};
}
echo $filecprint."</div>";
}

function wpc_auto_fancy_box() {
	if ( ( (is_page()) or (is_single()) ) and (!is_page(array('Albums','Messages','Documents','Equipe'))) ) { ?>
	    <script>
	    jQuery(document).ready(function(){
	        jQuery("#main").find("a:has(img)").addClass('fancybox');
			jQuery("#main").find("a:has(img)").attr('rel','group1');
        	        jQuery(".fancybox").fancybox( {
										'height':'80%',
										'width':'80%',
								    //'type':'iframe',
								  	'autoSize':false,
										 openEffect  : 'none',
												closeEffect : 'none',
												helpers : {
													media : {}
									}
				} );
	    });
	    </script>
<?php } }
add_action('wp_footer','wpc_auto_fancy_box');

class Description_Walker extends Walker_Nav_Menu
{
function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
	$filem = sprintf(''.theme_root().'/img/%s.svg', esc_html($item->attr_title));
	$filem2 = sprintf(''.theme_root().'/img/%s2.svg', esc_html($item->attr_title));
	$activeMenuItemClass = (in_array('current-menu-item', $item->classes)) ? ' class="nav-selected"' : '';
	global $wp_query;
    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

    // depth dependent classes
    $depth_classes = array(
        ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
        'menu-item-depth-' . $depth
    );
    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

    // passed classes
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

    // build html
    $output .= $indent . '<li class="' . $depth_class_names . ' ' . $class_names . ' menu-item-' . $item->ID . '">';
    // $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . $item->ID . '">';

    // link attributes
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
// notifications -->
	$notif = "";
	if ($item->attr_title == "fil-rouge") {
		$notif = "<div class='notif'>4</div>";
	}
	if ($item->attr_title == "messages") {
		$notif = "<div class='notif'>1</div>";
	}
//
	if ($activeMenuItemClass==='') {
	$args->before = "<a href='".$item->url."'><div class='col-xs-4 text-left menu-icon-m'><div class='menu-icon'><img src='".$filem."' data-src='".$filem."' data-alt-src='".$filem2."' alt='".$item->attr_title."' data-no-retina /></div>".$notif."</div></a>";
	//onerror='this.src='image.png'
	} else {
	$args->before = "<a href='".$item->url."'><div class='col-xs-4 text-left menu-icon-m'><div class='menu-icon'><img src='".$filem2."' alt='".$item->attr_title."' data-no-retina /></div>".$notif."</div></a>";
	};
	$item_output = ! empty( $args->before ) ? $args->before : '';
	$item_output .= '<a'. $attributes .'>';
	$item_output .= ! empty( $args->links_before ) ? $args->links_before : '<div class="col-xs-4 col-sm-7 text-center col-m"><div class="menu-text">';
	$item_output .= apply_filters( 'the_title', $item->title, $item->ID );
	$item_output .= ! empty( $args->links_after ) ? $args->links_after : '</div></div>';
	$item_output .= '</a>';
	$item_output .= ! empty( $args->after ) ? $args->after : '<div class="clearfix visible-xs-block"></div><div class="col-xs-4 visible-xs"></div>';
	//.file_get_contents($filem).
    //parent::start_el($output, $item, $depth, $args);
	$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}
}

function get_calendar2($initial = true, $echo = true) {
	global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;

	$key = md5( $m . $monthnum . $year );
	if ( $cache = wp_cache_get( 'get_calendar', 'calendar' ) ) {
		if ( is_array($cache) && isset( $cache[ $key ] ) ) {
			if ( $echo ) {
				/** This filter is documented in wp-includes/general-template.php */
				echo apply_filters( 'get_calendar', $cache[$key] );
				return;
			} else {
				/** This filter is documented in wp-includes/general-template.php */
				return apply_filters( 'get_calendar', $cache[$key] );
			}
		}
	}

	if ( !is_array($cache) )
		$cache = array();

	// Quick check. If we have no posts at all, abort!
	if ( !$posts ) {
		$gotsome = $wpdb->get_var("SELECT 1 as test FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'publish' LIMIT 1");
		if ( !$gotsome ) {
			$cache[ $key ] = '';
			wp_cache_set( 'get_calendar', $cache, 'calendar' );
			return;
		}
	}

	if ( isset($_GET['w']) )
		$w = ''.intval($_GET['w']);

	// week_begins = 0 stands for Sunday
	$week_begins = intval(get_option('start_of_week'));

	// Let's figure out when we are
	if ( !empty($monthnum) && !empty($year) ) {
		$thismonth = ''.zeroise(intval($monthnum), 2);
		$thisyear = ''.intval($year);
	} elseif ( !empty($w) ) {
		// We need to get the month from MySQL
		$thisyear = ''.intval(substr($m, 0, 4));
		$d = (($w - 1) * 7) + 6; //it seems MySQL's weeks disagree with PHP's
		$thismonth = $wpdb->get_var("SELECT DATE_FORMAT((DATE_ADD('{$thisyear}0101', INTERVAL $d DAY) ), '%m')");
	} elseif ( !empty($m) ) {
		$thisyear = ''.intval(substr($m, 0, 4));
		if ( strlen($m) < 6 )
				$thismonth = '01';
		else
				$thismonth = ''.zeroise(intval(substr($m, 4, 2)), 2);
	} else {
		$thisyear = gmdate('Y', current_time('timestamp'));
		$thismonth = gmdate('m', current_time('timestamp'));
	}

	$unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
	$last_day = date('t', $unixmonth);
	$previousmonth = gmdate('m', strtotime("-1 month"));
	$nextmonth = gmdate('m', strtotime("+1 month"));
	//$wp_locale->get_month_abbrev($wp_locale->get_month($previousmonth));
	//$wp_locale->get_month_abbrev($wp_locale->get_month($nextmonth));
	$calendar_prev = '<td colspan="1" id="prev"><a href="' . get_month_link($thisyear, $previousmonth) . '"><img src="'.theme_root().'/img/prev.png" alt="prev" /></a></td>';
	$calendar_next = '<td colspan="1" id="next"><a href="' . get_month_link($thisyear, $nextmonth) . '"><img src="'.theme_root().'/img/next.png" alt="next" /></a></td>';

	/* translators: Calendar caption: 1: month name, 2: 4-digit year */
	$calendar_caption = _x('%1$s %2$s', 'calendar caption');
	$calendar_output = '<table id="wp-calendar"><thead class="caption" ><tr>' . $calendar_prev .'
	<td colspan="5" >' . sprintf($calendar_caption, $wp_locale->get_month($thismonth), date('Y', $unixmonth)) . '</td>'. $calendar_next .'
	</tr></thead>
	<thead class="days">
	<tr>';



	$myweek = array();

	for ( $wdcount=0; $wdcount<=6; $wdcount++ ) {
		$myweek[] = $wp_locale->get_weekday(($wdcount+$week_begins)%7);
	}

	foreach ( $myweek as $wd ) {
		$day_name = (true == $initial) ?
		substr($wp_locale->get_weekday_abbrev($wd), 0, -1) : $wp_locale->get_weekday_initial($wd) ;
		$wd = esc_attr($wd);
		$calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\"><span class='cal-j'>$day_name</span></th>";
	}

	$calendar_output .= '
	</tr>
	</thead>

	<tfoot>
	<tr>';

	// Get the next and previous month and year with at least one post
	        $previous = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
	        FROM $wpdb->posts
	        WHERE post_date < '$thisyear-$thismonth-01'
            AND post_type = 'post' AND post_status = 'publish'
	        ORDER BY post_date DESC
	        LIMIT 1");
	        $next = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
	         FROM $wpdb->posts
	         WHERE post_date > '$thisyear-$thismonth-{$last_day} 23:59:59'
             AND post_type = 'post' AND post_status = 'publish'
	         ORDER BY post_date ASC
             LIMIT 1");

	if ( $previous ) {
//		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev"><a href="' . get_month_link($previous->year, $previous->month) . '">&laquo; ' . $wp_locale->get_month_abbrev($wp_locale->get_month($previous->month)) . '</a></td>';
	} else {
//		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev" class="pad">&nbsp;</td>';
	}

//	$calendar_output .= "\n\t\t".'<td class="pad">&nbsp;</td>';

	if ( $next ) {
//		$calendar_output .= "\n\t\t".'<td colspan="3" id="next"><a href="' . get_month_link($next->year, $next->month) . '">' . $wp_locale->get_month_abbrev($wp_locale->get_month($next->month)) . ' &raquo;</a></td>';
	} else {
//		$calendar_output .= "\n\t\t".'<td colspan="3" id="next" class="pad">&nbsp;</td>';
	}

	$calendar_output .= '<td colspan="1"></td><td colspan="3" class="imgcal-img-td"><img src="'.theme_root().'/img/imgcal.png" alt="imgcal" /></td><td colspan="3" class="imgcal-texte-td"><span class="imgcal-texte">Bel été !</span></td>
	</tr>
	</tfoot>

	<tbody>
	<tr>';

	$daywithpost = array();

	// Get days with posts
	$dayswithposts = $wpdb->get_results("SELECT DISTINCT DAYOFMONTH(post_date)
		FROM $wpdb->posts WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00'
		AND post_type = 'post' AND post_status = 'publish'
		AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59'", ARRAY_N);
	if ( $dayswithposts ) {
		foreach ( (array) $dayswithposts as $daywith ) {
			$daywithpost[] = $daywith[0];
		}
	}

	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'camino') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'safari') !== false)
		$ak_title_separator = "\n";
	else
		$ak_title_separator = ', ';

	$ak_titles_for_day = array();
	$ak_post_titles = $wpdb->get_results("SELECT ID, post_title, DAYOFMONTH(post_date) as dom "
		."FROM $wpdb->posts "
		."WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00' "
		."AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59' "
		."AND post_type = 'post' AND post_status = 'publish'"
	);
	if ( $ak_post_titles ) {
		foreach ( (array) $ak_post_titles as $ak_post_title ) {

				/** This filter is documented in wp-includes/post-template.php */
				$post_title = esc_attr( apply_filters( 'the_title', $ak_post_title->post_title, $ak_post_title->ID ) );

				if ( empty($ak_titles_for_day['day_'.$ak_post_title->dom]) )
					$ak_titles_for_day['day_'.$ak_post_title->dom] = '';
				if ( empty($ak_titles_for_day["$ak_post_title->dom"]) ) // first one
					$ak_titles_for_day["$ak_post_title->dom"] = $post_title;
				else
					$ak_titles_for_day["$ak_post_title->dom"] .= $ak_title_separator . $post_title;
		}
	}

	// See how much we should pad in the beginning
	$pad = calendar_week_mod(date('w', $unixmonth)-$week_begins);
	if ( 0 != $pad )
		$calendar_output .= "\n\t\t".'<td colspan="'. esc_attr($pad) .'" class="pad">&nbsp;</td>';

	$daysinmonth = intval(date('t', $unixmonth));
	for ( $day = 1; $day <= $daysinmonth; ++$day ) {
		if ( isset($newrow) && $newrow )
			$calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
		$newrow = false;

		if ( $day == gmdate('j', current_time('timestamp')) && $thismonth == gmdate('m', current_time('timestamp')) && $thisyear == gmdate('Y', current_time('timestamp')) )
			$calendar_output .= '<td id="today">';
		else
			$calendar_output .= '<td>';

		if ( in_array($day, $daywithpost) ) // any posts today?
				$calendar_output .= '<a href="' . get_day_link( $thisyear, $thismonth, $day ) . '" title="' . esc_attr( $ak_titles_for_day[ $day ] ) . "\">$day</a>";
		else
			$calendar_output .= '<span class="cal-j">' . $day . '</span>';
		$calendar_output .= '</td>';

		if ( 6 == calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins) )
			$newrow = true;
	}

	$pad = 7 - calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins);
	if ( $pad != 0 && $pad != 7 )
		$calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr($pad) .'">&nbsp;</td>';

	$calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table>";

	$cache[ $key ] = $calendar_output;
	wp_cache_set( 'get_calendar', $cache, 'calendar' );

	if ( $echo ) {
		/**
		 * Filter the HTML calendar output.
		 *
		 * @since 3.0.0
		 *
		 * @param string $calendar_output HTML output of the calendar.
		 */
		echo apply_filters( 'get_calendar', $calendar_output );
	} else {
		/** This filter is documented in wp-includes/general-template.php */
		return apply_filters( 'get_calendar', $calendar_output );
	}

}
/** calendar */
if (!function_exists('customBasicWidgetsInit')) {
	/**
	 * Register widget areas
	 */
	function customBasicWidgetsInit()
	{
		register_sidebar(array(
			'name'          => __('Header right', 'custom-basic'),
			'id'            => 'header-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Navigation bar right', 'custom-basic'),
			'id'            => 'navbar-right',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
		));

		register_sidebar(array(
			'name'          => __('Sidebar left', 'custom-basic'),
			'id'            => 'sidebar-left',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Sidebar right', 'custom-basic'),
			'id'            => 'sidebar-right',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Footer left', 'custom-basic'),
			'id'            => 'footer-left',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));

		register_sidebar(array(
			'name'          => __('Footer right', 'custom-basic'),
			'id'            => 'footer-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		));
	}// customBasicWidgetsInit
}
add_action('widgets_init', 'customBasicWidgetsInit');
function ajax_login_init(){
    wp_register_script('ajax-login-script', get_template_directory_uri() . '/js/theme-ajax.js', array('jquery') );
    wp_enqueue_script('ajax-login-script');
    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Veuillez patienter quelques instants...')
    ));
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}
function ajax_login(){
    check_ajax_referer( 'ajax-login-nonce', 'security' );
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;
    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        echo json_encode(array('loggedin'=>false, 'message'=>__('Mauvais mot de passe ou mauvaise adresse, veuillez réessayer')));
    } else {
        echo json_encode(array('loggedin'=>true, 'message'=>__('Connexion réussie, vous allez être redirigé dans quelques instants...')));
    }
    die();
}

if (!function_exists('customBasicEnqueueScripts')) {
	/**
	 * Enqueue scripts & styles
	 */
	function customBasicEnqueueScripts()
	{
		wp_enqueue_style('custom-style', get_template_directory_uri() . '/css/bootstrap.css');
		wp_enqueue_style('fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css');
		wp_enqueue_style('font1', 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');
		wp_enqueue_style('font2', 'http://fonts.googleapis.com/css?family=Pacifico:400');
		wp_enqueue_style('jquery-ui.css', get_template_directory_uri() . '/css/jquery-ui.css');
		wp_enqueue_style('jquery-ui.structure', get_template_directory_uri() . '/css/jquery-ui.structure.css');
		wp_enqueue_style('jquery-ui.theme', get_template_directory_uri() . '/css/jquery-ui.theme.css');
		wp_enqueue_style('fancybox-css', get_template_directory_uri() . '/css/jquery.fancybox.css');
		wp_enqueue_style('fancybox-css-buttons', get_template_directory_uri() . '/css/jquery.fancybox-buttons.css');
		wp_enqueue_style('fancybox-css-thumbs', get_template_directory_uri() . '/css/jquery.fancybox-thumbs.css');
		wp_enqueue_style('selectize', get_template_directory_uri() . '/css/selectize.css');
		wp_enqueue_style('app', get_template_directory_uri() . '/css/app.css');
		wp_enqueue_style('custom-basic-style', get_stylesheet_uri());

		wp_enqueue_script('modernizr-script', get_template_directory_uri() . '/js/modernizr.min.js');
		wp_enqueue_script('respond-script', get_template_directory_uri() . '/js/respond.min.js');
		wp_enqueue_script('html5-shiv-script', get_template_directory_uri() . '/js/html5shiv.js');
		wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-1.11.3.min.js');
		wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js');
		wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js');
		wp_enqueue_script('selectize', get_template_directory_uri().'/js/selectize.js');
		wp_enqueue_script('custom-script', get_template_directory_uri() . '/js/custom-script.js');
		wp_enqueue_script('fancybox', get_template_directory_uri().'/js/jquery.fancybox.js');
		wp_enqueue_script('fancybox-buttons', get_template_directory_uri().'/js/jquery.fancybox-buttons.js');
		wp_enqueue_script('fancybox-thumbs', get_template_directory_uri().'/js/jquery.fancybox-thumbs.js');
		wp_enqueue_script('fancybox-media', get_template_directory_uri().'/js/jquery.fancybox-media.js');

	}// customBasicEnqueueScripts
}
add_action('wp_enqueue_scripts', 'customBasicEnqueueScripts');
/**
 * admin page displaying help.
 */
if (is_admin()) {
//	require get_template_directory() . '/inc/BootstrapBasicAdminHelp.php';
//	$bbsc_adminhelp = new BootstrapBasicAdminHelp();
	//add_action('admin_menu', [$bbsc_adminhelp, 'themeHelpMenu']);
//	unset($bbsc_adminhelp);
}
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
/**
 * Custom dropdown menu and navbar in walker class
 */
require get_template_directory() . '/inc/BootstrapBasicMyWalkerNavMenu.php';
/**
 * Template functions
 */
require get_template_directory() . '/inc/template-functions.php';
/**
 * --------------------------------------------------------------
 * Theme widget & widget hooks
 * --------------------------------------------------------------
 */
require get_template_directory() . '/inc/widgets/BootstrapBasicSearchWidget.php';
require get_template_directory() . '/inc/template-widgets-hook.php';
/* */
