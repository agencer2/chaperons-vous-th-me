<?php
/**
 *  Les Petits Chaperons Rouges
 * Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title('|', true, 'right'); ?></title>
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">



		<!--wordpress head-->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<!--[if lt IE 8]>
			<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->
<div class="top-bar">
		<div class="container page-container">
			<?php do_action('before'); ?>
			<header>
				<div class="row row-with-vspace site-branding">
                                    <nav class="navbar navbar-default visible-xs-block">
                <div class="collapse navbar-collapse navbar-primary-collapse">
								<?php wp_nav_menu(array('theme_location' => 'primary', 'menu' => 'Menu 1', 'container' => false, 'menu_class' => 'nav navbar-nav', 'walker' => new Description_Walker)); ?>
                   </div><!--navbar-collapse-->
                   					</nav>
                    <div class="navbar-button col-xs-4 visible-xs">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-primary-collapse">
									<span class="sr-only"><?php _e('Toggle navigation', 'custom-basic'); ?></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
					</div>
					<div class="col-sm-4 site-title hidden-xs">
						<div class="select-creche-module">
							<div class="select-creche-container">
								<div class="select-creche-inner">
									<input type="text" class="select-creche-search" placeholder="Rechercher une crèche">
									<div class="select-creche-list">
										<ul>
											<li><a href="">Lorem.</a></li>
											<li><a href="">Consectetur.</a></li>
											<li><a href="">Vel.</a></li>
											<li><a href="">Excepturi.</a></li>
											<li><a href="">Alias?</a></li>
											<li><a href="">Distinctio.</a></li>
											<li><a href="">Fuga!</a></li>
											<li><a href="">Ullam.</a></li>
											<li><a href="">Sit.</a></li>
											<li><a href="">Minus.</a></li>
											<li><a href="">Cupiditate.</a></li>
											<li><a href="">Voluptatem.</a></li>
											<li><a href="">Natus.</a></li>
											<li><a href="">Repudiandae.</a></li>
											<li><a href="">Quia?</a></li>
											<li><a href="">Non!</a></li>
											<li><a href="">Voluptatum!</a></li>
											<li><a href="">Nisi.</a></li>
											<li><a href="">Tempore.</a></li>
											<li><a href="">Dolorum.</a></li>
											<li><a href="">Distinctio.</a></li>
											<li><a href="">Eius.</a></li>
											<li><a href="">Repudiandae!</a></li>
											<li><a href="">Repudiandae.</a></li>
											<li><a href="">Earum.</a></li>
											<li><a href="">Eius.</a></li>
											<li><a href="">Quod!</a></li>
											<li><a href="">Architecto?</a></li>
											<li><a href="">Quas?</a></li>
											<li><a href="">Quae?</a></li>
										</ul>									
									</div>
	
								</div>
							</div>
						</div>
						<h1 class="site-title-heading">
							<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a>
						</h1>
						<div class="site-description">
								<a class="action-toggle-select-creche"><?php bloginfo('description'); ?></a> 
						</div>
                    </div>
                    <div class="col-xs-4 col-sm-4 text-center">
						<div class="logo">
                        	<img src="<?php echo theme_root();?>/img/logo.png" alt="logo les petits chaperons rouges" />
                        </div>
					</div>
                    <div class="col-xs-4 visible-xs"></div>
                    <div class="col-xs-12 site-title visible-xs text-center headingm">
						<h1 class="site-title-heading">
							<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a>
						<span class="site-description">
								<?php bloginfo('description'); ?>
						</span></h1>
						<div class="site-description-m">
								<?php bloginfo('description'); ?>
						</div>

                    </div>
                    <div class="clearfix visible-xs-block"></div>
                    <div class="col-xs-6 col-sm-4 hidden-xs">
                    <div id="user">
                          <div class="iconeprofil"><img src="<?php echo theme_root();?>/img/iconeprofil.png" alt="icone profil" /></div>
                          <?php   $current_user = wp_get_current_user();if ( !($current_user instanceof WP_User) ) return;
						  if ( is_user_logged_in() ) {
													$author_id = $current_user->ID; $attachment_id = get_user_option( 'metronet_image_id', $author_id ); $urlp = rtrim(wp_get_attachment_url( $attachment_id ), ".jpg");
							if ( $urlp !== '' ) {
                          echo "<div class='nomprofil'>".$current_user->user_firstname .' '. $current_user->user_lastname."</div>";
                            echo "<div class='profil'>";
							?>
                            <div class="profil-p">
                                <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask1" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m1" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m1-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php $author_id = $current_user->ID; $attachment_id = get_user_option( 'metronet_image_id', $author_id ); echo rtrim(wp_get_attachment_url( $attachment_id ), ".jpg"); ?>-160x160.jpg" class="attachment-profil-size" mask="url(#svgmask1)" id="p1" y="-10%" x="-10%" width="125%" height="125%" />
									<image xlink:href="<?php $author_id = $current_user->ID; $attachment_id = get_user_option( 'metronet_image_id', $author_id ); echo rtrim(wp_get_attachment_url( $attachment_id ), ".jpg"); ?>-160x160@2x.jpg" class="attachment-profil-size" mask="url(#svgmask1)" id="p1-2x" y="-10%" x="-10%" width="125%" height="125%" />
                            </svg>
                            </div></div><?php } else {
															echo "<div class='nomprofil'>".$current_user->user_firstname .' '. $current_user->user_lastname."</div>";
																echo "<div class='profil'>";
									?>
																<div class="profil-p">
																		<svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
																				<defs>
																						<mask id="svgmask1" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
																						<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m1" />
												<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m1-2x" />
																						</mask>
																				</defs>
																				<image xlink:href="<?php echo theme_root() ?>/img/profil-defaut.png" class="attachment-profil-size" mask="url(#svgmask1)" data-no-retina y="0%" x="0%" width="100%" height="100%" />
																</svg>
															</div></div><?php };
 } ?>

                    </div>
                    <div id="user-menu">
                    	<div class="user-menu-top">
                    	</div>
                    	<div class="user-menu-cadre">


                    		<div class="user-menu-profil">

                        		<div class="profil-user-i">
															<a href="./wp-admin/profile.php">
																<img src="<?php echo theme_root()?>/img/user-i.svg" alt="Menus" data-no-retina />
                                </div>
                                <div class="user-menu-nom">Mon profil</div>
															</a>

                    		</div><!--/user-menu-profil-->

                    		<?php
                    		if (isset($_SESSION['children'])) {
                    			foreach ($_SESSION['children'] as $child) {
                    				echo '<div class="user-menu-profil">';
                    					echo '<div class="profil-user">';
                    						echo '<svg width="50" height="34" baseProfile="full" preserveAspectRatio="none">';
                    							echo '<defs>';
                    								echo '<mask id="svgmasku2" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">';
                    									echo '<image width="100%" height="100%" xlink:href="'.theme_root().'/img/mask.png" id="mu2" />';
                    									echo '<image width="100%" height="100%" xlink:href="'.theme_root().'/img/mask@2x.png" id="mu2-2x" />';
                    								echo '</mask>';
                    							echo '</defs>';
                    							echo '<image xlink:href="'.theme_root().'/img/exemples/user2.jpg" class="attachment-profil-size" mask="url(#svgmasku2)" id="pu2" y="0%" x="0%" width="100%" height="100%" />';
                    							echo '<image xlink:href="'.theme_root().'/img/exemples/user2@2x.jpg" class="attachment-profil-size" mask="url(#svgmasku2)" id="pu2-2x" y="0%" x="0%" width="100%" height="100%" />';
                    						echo '</svg>';
                    					echo '</div>';
                    					echo '<div class="user-menu-nom">'.$child->get_first_name().' '.$child->get_last_name().'</div>';
                    				echo '</div>';
                    			}
                    		}
                    		?>

                    		<!-- <div class="user-menu-profil">

                        		<div class="profil-user">
                                <svg width="50" height="34" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmasku2" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php // echo theme_root();?>/img/mask.png" id="mu2" />
										<image width="100%" height="100%" xlink:href="<?php // echo theme_root();?>/img/mask@2x.png" id="mu2-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php // echo theme_root(); ?>/img/exemples/user2.jpg" class="attachment-profil-size" mask="url(#svgmasku2)" id="pu2" y="0%" x="0%" width="100%" height="100%" />
									<image xlink:href="<?php // echo theme_root(); ?>/img/exemples/user2@2x.jpg" class="attachment-profil-size" mask="url(#svgmasku2)" id="pu2-2x" y="0%" x="0%" width="100%" height="100%" />
                                </svg>
								</div>
                                <div class="user-menu-nom">Léo Philippe</div>

                    		</div> --><!--/user-menu-profil-->


                    		<div class="user-menu-profil">

                        		<div class="profil-user-i">
								<img src="<?php echo theme_root()?>/img/cgu-i.svg" alt="Menus" data-no-retina />
								</div>
                                <div class="user-menu-nom">CGU</div>

                    		</div><!--/user-menu-profil-->



                    		<div class="user-menu-profil">
														<a href="<?php echo wp_logout_url(home_url()); ?>">
                        				<div class="profil-user-lock">
								<img src="<?php echo theme_root()?>/img/lock-i.svg" alt="Menus" data-no-retina />
								</div>
                                <div class="user-menu-nom">Se déconnecter</div>
														</a>

                    		</div><!--/user-menu-profil-->





                    	</div><!--/user-menu-cadre-->
                    </div>
                    </div>

				</div><!--site-branding-->
                </header>
		</div>
</div>
<div class="container page-container">
			<div id="content" class="row row-with-vspace site-content">
