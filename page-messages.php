<?php
/**
/* Template Name: Messages */
/* Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
get_header();
?>
<?php get_sidebar('left'); ?>
				<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
					<main id="main" class="site-main">
                    <div class="content-top"></div>

                    	<!-- formulaire d'envoi -->
											<form action="" method="post" name="messagesform" id="messagesform">

                        <div class="cadre-post col-xs-12 col-sm-12 col-md-12"><div class="messages-form">
                        <div class='post-title-m'><h2><a href="#">Contacter la crèche</a></h2></div>

                        <div class="messages-contact-email">

												<div class="contact-email">
													<label for="select_email">À :</label>
													<select name="email" class="selectize message" id="select_email" placeholder="Destinataire">
													</select>
												</div>

												</div>
                        <div class="messages-contact-objet">

													<input type="text" class="contact-objet-input" name ="object" id="object" value="Objet : " />

												</div>
                        <div class="messages-contact-contenu">

													<textarea class="contact-contenu-textarea" rows="9" name="message" placeholder="Message : " id="message" ></textarea>

												</div>

													<input type="hidden" name="file" value="" id="file" />

                        <div class="visible-xs joindre-xs"><div class="messages-joindre" onClick="?" ><div class="col-xs-4 messages-joindre-i"><img src="<?php echo theme_root()."/img/joindre-i.png"; ?>"  alt="joindre" /></div><div class="col-xs-4 messages-joindre-t">Joindre un fichier</div></div></div>

													<input type="hidden" name="submit" value="submit" />
                        <div class="messages-envoyer" id="submit" onClick="document.forms['messagesform'].submit();" ><div class="col-xs-4 messages-envoyer-i"><img src="<?php echo theme_root()."/img/envoyer-i.svg"; ?>"  alt="envoyer" data-no-retina /></div><div class="col-xs-4 messages-envoyer-t">Envoyer</div></div>

                        <div class="hidden-xs"><div class="messages-joindre" onClick="?" ><div class="col-xs-4 messages-joindre-i"><img src="<?php echo theme_root()."/img/joindre-i.png"; ?>" alt="joindre" /></div><div class="col-xs-4 messages-joindre-t">Joindre un fichier</div></div></div>

						<div class="hidden-xs"><br /><br /></div>

                        </div></div>
						<!-- /formulaire d'envoi -->


						<!-- nouveau message -->
                        <div class="cadre-post col-xs-12 col-sm-12 col-md-12"><div class="messages-post">

                            <div class="profil-p">
                                <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask2" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m2" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m2-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/2.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask2)" id="p2" y="-10%" x="-10%" width="125%" height="125%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/2@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask5)" id="p2-2x" y="-10%" x="-10%" width="125%" height="125%" />

                            </svg>
                            </div>

						<div class="messages-nom">De : Alice Durand</div><div class="messages-email"><a href="">alice.durand@lcpr.fr</a></div>
                        <div class="messages-objet">Objet : Reunion rentrée</div>
                        <div class="icone-messages"><img src="<?php echo theme_root();?>/img/iconeprofil-r.png" alt="icone profil" /></div>
                        <div class="date-messages hidden-xs hidden-sm">Le 22 juillet à 9h48</div>
                        <div class="contenu-messages">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam finibus tellus a tellus malesuada maximus. Fusce consectetur dolor nec neque fringilla tincidunt nec ac lacus.<br /><br />Phasellus ac magna dictum, posuere ligula ac, faucibus est. Mauris non eros vel urna porta blandit a vitae metus. Nullam varius leo id metus luctus hendrerit. Pellentesque egestas, velit at tempor blandit, justo tellus blandit nisi, dignissim tincidunt dolor ipsum eget nisi. Aenean urna nisl, lobortis in aliquet a, ultrices nec quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                        <div class="messages-repondre"><div class="col-xs-4 messages-envoyer-i"><img src="<?php echo theme_root()."/img/envoyer-i.svg"; ?>" alt="répondre av" data-no-retina /><img src="<?php echo theme_root()."/img/repondre-i.svg"; ?>" alt="répondre" class="repondre-i" data-no-retina /></div><div class="col-xs-4  messages-repondre-t">Répondre</div><div class="col-xs-4"></div></div>
                        <div class="hidden-xs"><br /><br /></div>

                        </div></div>
                        <!-- /nouveau message -->


                        <!-- autres messages -->
                         <div class="cadre-post col-xs-12 col-sm-12 col-md-12"><div class="messages-post">

                            <div class="profil-p">
                                <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask3" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="mp3" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="mp3-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/1.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask3)" id="p3" y="-10%" x="-10%" width="125%" height="125%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/1@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask3)" id="p3-2x" y="-10%" x="-10%" width="125%" height="125%" />

                            </svg>
                            </div>

						<div class="messages-nom">De : Pierre Dupont</div><div class="messages-email"><a href="">pierre.dupont@lcpr.fr</a></div>
                        <div class="messages-objet">Objet : Loren ipsum dolor sit amet, consetetur</div>
                        <div class="icone-messages"><img src="<?php echo theme_root();?>/img/iconeprofil.png" alt="icone profil" /></div>
                        <div class="date-messages hidden-xs hidden-sm">Le 22 juillet à 9h48</div>

                        </div></div><!--  -->


                         <div class="cadre-post col-xs-12 col-sm-12 col-md-12"><div class="messages-post">

                            <div class="profil-p">
                                <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask4" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="mp4" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="mp4-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/6.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask4)" id="p4" y="-10%" x="-10%" width="125%" height="125%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/6@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask4)" id="p4-2x" y="-10%" x="-10%" width="125%" height="125%" />
                            </svg>
                            </div>

						<div class="messages-nom">De : Jane Doe</div><div class="messages-email"><a href="">jane.doe@lcpr.fr</a></div>
                        <div class="messages-objet">Objet : Réunion rentrée</div>
                        <div class="icone-messages"><img src="<?php echo theme_root();?>/img/iconeprofil.png" alt="icone profil" /></div>
                        <div class="date-messages hidden-xs hidden-sm">Le 19 juillet à 11h36</div>

                        </div></div><!--  -->


                         <div class="cadre-post col-xs-12 col-sm-12 col-md-12"><div class="messages-post">

                            <div class="profil-p">
                                <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask5" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="mp5" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="mp5-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo theme_root()."/img/profils/4.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask5)" id="p5" y="-10%" x="-10%" width="125%" height="125%" />
									<image xlink:href="<?php echo theme_root()."/img/profils/4@2x.jpg"; ?>" class="attachment-profil-size" mask="url(#svgmask5)" id="p5-2x" y="-10%" x="-10%" width="125%" height="125%" />
                            </svg>
                            </div>

						<div class="messages-nom">De : Emmanuelle Dubois</div><div class="messages-email"><a href="">emmanuelle.dubois@lcpr.fr</a></div>
                        <div class="messages-objet">Objet : Loren ipsum dolor sit amet, consetetur</div>
                        <div class="icone-messages"><img src="<?php echo theme_root();?>/img/iconeprofil.png" alt="icone profil" /></div>
                        <div class="date-messages hidden-xs hidden-sm">Le 15 juillet à 18h03</div>

                        </div></div>
						<!-- /autres messages -->


					</main>
                    </div>

				</div>
                <div class="clearfix"></div>
                <br />
<?php //get_sidebar('right'); ?>
             <!--site-content-->
<?php get_footer(); ?>
