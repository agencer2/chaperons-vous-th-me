<?php
/**
/* Template Name: Albums */
/* R2 Agence Digitale.
*/
get_header();
?>
<?php get_sidebar('left'); ?>
<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
    <main id="main" class="site-main">
    <div class="content-top"></div>
    <div class="cadre-top-title2">

        <div class="album-header">
            <div class="row">
                <div class="pull-right">
                    <a href="#" class="button-md red js-create-album">Créer un album</a>
                </div>
            </div>
        </div>


        <div class="cadre-post cadre-cat">
            <div class="profil-post col-xs-12" >
                <div class="row album-image-list">
                    <?php $i = 1;
                    while($i < 10){ ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 album-item-container" data-photo-id="<?php echo $i ?>">
                        <div class="album-item">
                            <a class="album-item-link" href="#">
                                <span class="album-item-img">
                                    <img class="img-responsive" src="http://lorempicsum.com/up/310/310/<?php echo $i; ?>" alt="">
                                </span>
                                <h4>Photos de bébé</h4>
                                <span class="album-item-photos-counter"><?php echo rand(1, 50) ?> photos</span>
                            </a>
                            <div class="album-item-controls">
                                <ul>
                                    <li><a href="#" class="js-album-download-photo" data-photo-id="<?php echo $i; ?>"><i class="icon icon-download"></i></a></li>
                                    <li><a href="#" class="js-album-delete-photo" data-photo-id="<?php echo $i; ?>"><i class="icon icon-trash"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                </div>
            </div>
        </div>
    </div>
    </main>
</div>
<div class="c-modal-binder">
    <div class="c-modal-overlay modal--create-album">
        <div class="c-modal-container">

            <div class="col-xs-12">
                <div class="c-modal-header">
                    <div class="row">
                        <h1>Créer un nouvel album</h1>
                    </div>
                    <div class="row">
                        <fieldset>
                            <label for="">Titre de l'album</label>
                            <input type="text" class="form-control js-form-create-album-name" autofocus placeholder="Nom du nouvel album">
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                <?php $i = 1;
                    while($i < 6){ ?>
                    <div class="col-md-4 col-sm-4 col-xs-6 album-item-container" data-photo-id="<?php echo $i ?>">
                        <div class="album-item">
                            <a class="album-item-link" href="#">
                                <span class="album-item-img">
                                    <img class="img-responsive" src="http://lorempicsum.com/simpsons/310/310/<?php echo $i; ?>" alt="">
                                </span>
                            </a>
                            <div class="album-item-controls">
                                <ul>
                                    <li><a href="#" class="js-album-delete-photo" data-photo-id="<?php echo $i; ?>"><i class="icon icon-trash"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <div class="col-md-4 col-sm-4 col-xs-6">

                        <a href="" class="album-modal-add-zone">
                            <span class="icon icon-add-photo"></span>
                            Ajouter des photos
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="c-modal-footer">
                <div class="pull-right">
                    <a class="button-md grey modal--close-album" href="">Annuler</a>
                    <a class="button-md red js-create-album-submit" href="">Créer l'album</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>




<?php get_footer(); ?>