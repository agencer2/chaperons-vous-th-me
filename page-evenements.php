<?php
/**
/* Template Name: Evenements */
/* Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
get_header();
?> 
<?php get_sidebar('left'); ?> 

                <div class="col-xs-12 col-sm-8 col-md-3 side">
                <div class="vue-cal-i"><img src="<?php echo theme_root();?>/img/vue-cal-i.png" alt="vue cal" /></div><div class="vue-cal">Vue calendrier</div>
                <div class="vue-fil-i hidden"><img src="<?php echo theme_root();?>/img/vue-fil-i.png" alt="vue fil" /></div><div class="vue-cal hidden">Vue fil</div>
                <?php get_calendar2(); ?><!-- fonction calendrier wp avec quelques ajustements, voir functions.php -->
                </div>

			<main id="main" class="site-main">

				<div class="col-xs-12 col-sm-8 col-md-6 evenements" id="main-column">
                
                    <div class="content-top"></div>
                    <div class="cadre-top-title">
                    <div class="top-title">Juillet 2015</div>
                    </div>
                    
                    <!-- évènements du mois -->
                    <div class="evenements-post col-xs-12 col-sm-12 col-md-12 separation">
                    
                    <div class="top-title">Samedi 25 Juillet</div><div class="evenements-tag">Activités Parents</div><div class="clearfix"></div>
                    <div class="evenements-titre">Les Petits Chaperons Rouges au Corporate Game</div>
                    <div class="evenements-miniature"><img src="<?php echo theme_root();?>/img/exemples/e1.jpg" alt="loren ipsum" /></div>
                    <div class="evenements-contenu">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</div>
                    <div class="evenements-lien"><a href="#">Participer à l’événement</a></div>
                   
                    </div>
                    
                    <div class="evenements-post-bas col-xs-12 col-sm-12 col-md-12">
                    
                    <div class="top-title">Mardi 28 Juillet</div><div class="evenements-tag">Anniversaire</div><div class="clearfix"></div>
                    <div class="evenements-titre">La Crèche de Montrouge souffle sa 4e bougie</div>
                    <div class="evenements-miniature"><img src="<?php echo theme_root();?>/img/exemples/e2.jpg" alt="loren ipsum" /></div>
					<div class="evenements-contenu">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</div>
                    <div class="evenements-lien"><a href="#">Participer à l’événement</a></div>
                    </div><div class="clearfix"></div>
                    <!-- /évènements du mois -->
                    
                    
                    <!-- évènements du mois dernier -->
                    <div class="content-top"></div>
                    <div class="cadre-top-title">
                    <div class="top-title">Août 2015</div>
                    </div>
                    
                    <div class="evenements-post col-xs-12 col-sm-12 col-md-12">
                    <div class="evenements-vide-img"><img width="116" height="110" src="<?php echo theme_root();?>/img/evenements-vide.png" alt="evenements vide" /></div>
                    <div class="evenements-vide-texte">Aucun évènement pour l'instant</div>
                    </div><div class="clearfix"></div>
                    <!-- /évènements du mois dernier -->
                
                	<!-- évènements du mois avant-dernier -->
                    <div class="content-top"></div>
                    <div class="cadre-top-title">
                    <div class="top-title">Septembre 2015</div>
                    </div>
                    
                    <div class="evenements-post col-xs-12 col-sm-12 col-md-12">
                    <div class="evenements-vide-img"><img src="<?php echo theme_root();?>/img/evenements-vide.png" alt="evenements vide" /></div>
                    <div class="evenements-vide-texte">Aucun évènement pour l'instant</div>
                    </div><div class="clearfix"></div>
                    <!-- /évènements du mois avant-dernier -->
                    <br /><br />
                    
                 </div><!-- main column -->   
                 
                 
                 <br /><br />

                
			</main>
		</div>
<?php get_footer(); ?> 