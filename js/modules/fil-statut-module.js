(function() {


  'use strict';

  var window,
      document,
      $ = jQuery.noConflict();

  var s;
  app.SampleWidget = {

    settings: {
      output: 'What did you do man? Let me slee!',
      elBody: $('body')
    },

    init: function() {
      s = this.settings;
      this.bindUIActions();
      $('.selectpicker').selectpicker({
	      style: 'select-md',
	      size: 4
	  });
    },

    bindUIActions: function() {
      var self = this;

      s.elBody.bind('click', function() {
      });
    },

    returnUserEvent: function(outputReturn) {
      app.console(outputReturn);
    }
  };

  app.SampleWidget.init();
})();