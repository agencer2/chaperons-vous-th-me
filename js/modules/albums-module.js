(function() {
  'use strict';

  var document,
      $ = jQuery.noConflict();

  var s;
  app.AlbumModule = {

    settings: {
      modalObject: $('.modal--create-album'),
      albumContainer: $('.album-image-list'),
      closeModal: $('.modal--close-album'),
      triggerModal: $('.js-create-album'),
      formAlbumName: $('.js-form-create-album-name'),
      deletePhoto: $('.js-album-delete-photo'),
      submit: $('.js-create-album-submit'),
      token: Math.random().toString(36).substr(2),
      elBody: $('body'),      
      createAlbumEndpoint: '/wp-content/chaperons-vous-wp-plugins/public/ajax/album_create.php',
      addPhotoEndpoint: '/wp-content/chaperons-vous-wp-plugins/public/ajax/album_upload.php'
    },

    init: function() {
      s = this.settings;

      this.bindUIActions();
      this.bindUIEvents();
    },

    bindUIActions: function() {
      var self = this;

      s.triggerModal.bind('click', function(event) {
        event.preventDefault();
        self.showModal();
      });

      s.closeModal.bind('click', function(event) {
        event.preventDefault();
        self.closeModal();
      });

      s.deletePhoto.live('click', function(event) {
        var photo_id = $(this).attr('data-photo-id');
        self.deletePhoto(photo_id, this);
        event.preventDefault();
      });

      s.submit.bind('click', function(event) {
        event.preventDefault();
        $(this).css('opacity', '0.6')
        var button = this;

        setTimeout(function(){
          self.submitModal();
          $(button).css('opacity', '1')
        }, 1500);
      });
    },
    bindUIEvents: function() {
      var self = this;

      $(window).keyup(function(e) {
           if (e.keyCode == 27) { 
              self.closeModal();
          }
            if (e.keyCode == 13) { 
              self.submitModal();
          }
      });
    },
    showModal: function() {
      s.modalObject.addClass('c-modal-visible');
      s.formAlbumName.focus();
    },
    closeModal: function(outputReturn) {
      s.modalObject.removeClass('c-modal-visible');
    },
    deletePhoto: function(photo_id, element) {
      $(element).closest('.album-item-container').remove();
    },
    clearModal: function(outputReturn) {
      app.console(outputReturn);
    },
    submitModal: function() {

      var self = this;

      $.ajax({
        type: "POST",
        url: s.createAlbumEndpoint,
        data: {name: s.formAlbumName.val(), albumToken: s.token},
        success: success
      });

      // Uniquement pour le debug
      self.createAlbum();

      function success(){
          self.createAlbum();
      }
    },
    createAlbum: function() {
        var self = this;

        self.closeModal();
        $('.album-item-container:last-child').clone().appendTo(s.albumContainer);
    }
  };

  app.AlbumModule.init();
})();