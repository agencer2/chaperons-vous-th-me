(function() {
  'use strict';

  var window,
      document,
      $ = jQuery.noConflict();

  var s;
  app.SelectCrecheModule = {

    settings: {
      trigger: $('.action-toggle-select-creche'),
      crecheContainer: $('.select-creche-container'),
      searchInput: $('.select-creche-search'),
      elBody: $('body')
    },

    init: function() {
      s = this.settings;

      this.bindUIActions();
    },

    bindUIActions: function() {
      var self = this;

      s.trigger.bind('click', function(event) {
        event.preventDefault();
        self.toggleSelect();
      });
    
      s.crecheContainer.bind('click', function(event) {
        event.preventDefault();
        self.toggleSelect();
      });
    },

    toggleSelect: function(outputReturn) {

        s.searchInput.focus();
        $('.select-creche-module').toggleClass('select-creche-module-visible');
      
    },

    hideSelect: function(outputReturn) {

      app.console('hide');

          s.searchInput.focus();
          //$('.select-creche-module').removeClass('select-creche-module-visible');

    },

    search: function(string) {

    },
  };

  app.SelectCrecheModule.init();
})();