jQuery(function($){
    $('form#rsUserRegistration').on('submit', function(e){
        $('#preloader').stop().fadeIn(500);
        $('#message').stop().hide().fadeIn(500).text(ajax_login_object.loadingmessage);
        if ($('form#rsUserRegistration #log').val() == "") {
        $('#message').stop().hide().text("Veuillez renseigner votre adresse email").fadeIn(500);
        $('#preloader').stop().fadeOut(500);
        } else {
        if ($('form#rsUserRegistration #pwd').val() == "") {
        $('#message').stop().hide().text("Veuillez renseigner votre mot de passe").fadeIn(500);
        $('#preloader').stop().fadeOut(500);
        } else {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin',
                'username': $('form#rsUserRegistration #log').val(),
                'password': $('form#rsUserRegistration #pwd').val(),
                'security': $('form#rsUserRegistration #security').val() },
            success: function(data){
                $('#preloader').stop().fadeOut(250);
                $('#message').stop().hide().text(data.message).fadeIn(500);
                if (data.loggedin == true){
                    document.location.href = ajax_login_object.redirecturl;
                }
            }
        });
      }}
        e.preventDefault();
    });
});
