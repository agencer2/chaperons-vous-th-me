<?php
/**
/* Template Name: Accueil
 * Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title('|', true, 'right'); ?></title>
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes">

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!--wordpress head-->
		<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
		<!--[if lt IE 8]>
			<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->

<div class="top-accueil">

	<div class="container page-container">
					<div class="logo">
                        	<img src="<?php echo theme_root();?>/img/logo.png" alt="logo les petits chaperons rouges" />
					</div>
     </div>

</div>
<div class="bg-accueil" id="b1">

			<div class="container-accueil">
					<div class="icons-g">
						<div class="icons-g2">
							<div class="icons-i"><a href="#"><div class="icons-wrapper"><img src="<?php echo theme_root()?>/img/evenements-b.svg" alt="Evenements" data-no-retina /></div></a></div>
							<a href="#"><div class="icons-t">Événements</div></a>
						</div>
						<div class="icons-g1">
							<div class="icons-i"><a href="#"><div class="icons-wrapper"><img src="<?php echo theme_root()?>/img/albums-b.svg" alt="Albums" data-no-retina /></div></a></div>
							<a href="#"><div class="icons-t">Albums</div></a>
						</div>
					</div>
					<div class="icons-d">
						<div class="icons-g3">
							<div class="icons-i"><a href="#"><div class="icons-wrapper"><img src="<?php echo theme_root()?>/img/articles-b.svg" alt="Articles" data-no-retina /></div></a></div>
							<a href="#"><div class="icons-t">Articles</div></a>
						</div>
						<div class="icons-g4">
							<div class="icons-i"><a href="#"><div class="icons-wrapper"><img src="<?php echo theme_root()?>/img/messages-b.svg" alt="Messages" data-no-retina /></div></a></div>
							<a href="#"><div class="icons-t">Messages</div></a>
						</div>
					</div>
    				<div class="cadre-connec">
    					<div class="inner">
	    					<div class="post-title">
									<div class="titre-connec">
									<h2>Se connecter</h2>
									</div>
									<div class="logo">
									<img src="<?php echo theme_root();?>/img/logo.png" alt="logo les petits chaperons rouges" />
									</div>
								</div>
								<div class="userRegistration">
								<form name="loginform" id="rsUserRegistration" method="post" >
	    					<div class="input-connec"><input type="text" placeholder="Email : " id="log" name="log" /></div>
	    					<div class="input-password"><input type="password" placeholder="Mot de passe : " id="pwd" name="pwd" /></div>
	    					<div class="password-link"><a href="<?php echo wp_lostpassword_url(); ?>">Mot de passe oublié ?</a></div>
	    					<div class="clearfix"></div>
								<input type="hidden" name="remember" id="remember" value="true" />
	    					<button type="submit" class="bouton-connec" name="wp-submit" id="submit"><div class="col-xs-4 bouton-connec-i"><img src="<?php echo theme_root()."/img/connexion.svg"; ?>"  alt="connexion" data-no-retina /></div><div class="col-xs-4 bouton-connec-t">Connexion</div></button>
								<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
								</form>
								</div>
								</div>
								<div class="clearfix"></div>
								<div class="plder"><img src="<?php echo theme_root()?>/img/ajax-loader.gif" id="preloader" alt="Preloader" /></div>
								<div class="messagediv">
								<div id="message" class="login-msg">
									<?php $current_user = wp_get_current_user();if ( !($current_user instanceof WP_User) ) return;
									if (is_user_logged_in()) { echo "Bienvenue ".$current_user->user_firstname; ?>, vous allez être redirigé dans quelques instants...
									<?php } else {} ?>
								</div>
								</div>

    				</div>

    				<div class="topfil-b">
										<div class="clearfix"></div>
                    <img src="<?php echo theme_root(); ?>/img/filrouge-b.png" alt="fil rouge b" />
                    </div>
                    <div class="texte-menu">
                    Les Petits Chaperons Rouges conçoivent, réalisent, animent et gèrent des crèches…
                    <br />
                    <a href="http://www.lpcr.fr" target="_blank">En savoir +</a>
                    </div>
										<br /><br />
			</div>


<?php if (is_user_logged_in()) { ?>
<script type="text/javascript" language="JavaScript">
setTimeout(function () { location.href = "<?php echo esc_url(home_url('/')); ?>";}, 7000);
</script>
<?php } get_footer(); ?>
