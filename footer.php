<?php
/**
 * The theme footer
 * Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
?>

				<br />
			</div>
		<!--	<footer id="site-footer" role="contentinfo">
				<div id="footer-row" class="row site-footer">
					<div class="col-md-6 footer-left">Copyright Lespetitschaperonsrouges.fr 2015 - Tous droits réservés</div>
					<div class="col-md-6 footer-right text-right">
						<?php //dynamic_sidebar('footer-right'); ?> 
					</div>
				</div>
			</footer>
		</div>--><!--.container page-container-->
		
		
		<!--wordpress footer-->
        
		<?php wp_footer(); ?> 

		<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/bootstrap-select.min.js"></script>

		<?php foreach(glob(get_template_directory().'/js/modules/*.js', GLOB_BRACE) as $js){ ?>
			<script src="<?php echo get_template_directory_uri(); ?>/js/modules/<?php echo basename($js) ?>"></script>
		<?php } ?>

	</body>
</html>