<?php
/**
 * For store widget hook action/filter.
 * Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */


if (!function_exists('customBasicWidgetHooksGetCalendar')) {
	/**
	 * change WordPress calendar widget table to add custom class into it.
	 * 
	 * @todo change code in this function when WordPress allowed to hook into that widget.
	 * @param string $calendar
	 * @return string
	 */
	function customBasicWidgetHooksGetCalendar($calendar) 
	{
		$new_calendar = preg_replace('#(<table*\s)(id="wp-calendar")#i', '$1 id="wp-calendar" class="table"', $calendar);

		return $new_calendar;
	}// customBasicWidgetHooksGetCalendar
}
add_filter('get_calendar', 'customBasicWidgetHooksGetCalendar', 10, 1);

