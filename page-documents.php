<?php
/**
/* Template Name: Documents */
/* Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */
get_header();
?> 
<?php get_sidebar('left'); ?> 
				<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
					<main id="main" class="site-main">
                    <div class="content-top"></div>
                    
                        <div class="cadre-post-doc col-xs-12 col-sm-12 col-md-12">
                        
                        <div class="doc-icon"><img src="<?php echo theme_root()?>/img/doc-menu.svg" alt="Menus" data-no-retina /></div>
                        <div class="doc-title">Menus</div>

                        <div class="doc-ligne separation-gris-clair">
                        <div class="lien-fichier"><a href="#">Menu du 24 au 30 avril</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">176ko</div>
                        </div>
                        <div class="doc-ligne">
                        <div class="lien-fichier"><a href="#">Menu du 16 au 22 avril</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">247ko</div>
                        </div>

                        </div><!-- cadre post -->
                        
                        
                        <div class="cadre-post-doc col-xs-12 col-sm-12 col-md-12">
                        
                        <div class="doc-icon"><img src="<?php echo theme_root()?>/img/doc-news.svg" alt="Newsletters" data-no-retina /></div>
                        <div class="doc-title">Newsletters</div>
                        
                        <div class="doc-ligne separation-gris-clair">
                        <div class="lien-fichier"><a href="#">Newsletter du mois de juin 2015</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">323ko</div>
                        </div>
                        <div class="doc-ligne">
                        <div class="lien-fichier"><a href="#">Newsletter du mois de mai 2015</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">276ko</div>
                        </div>

                        </div><!-- cadre post -->
                        
                        
                        <div class="cadre-post-doc col-xs-12 col-sm-12 col-md-12">
                        
                        <div class="doc-icon"><img src="<?php echo theme_root()?>/img/doc-admin.svg" alt="Documents administratifs" data-no-retina /></div>
                        <div class="doc-title">Documents administratifs Crèche de Montrouge</div>
                        
                        <div class="doc-ligne">
                        <div class="lien-fichier"><a href="#">Formulaire d'inscription</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">108ko</div>
                        </div>

                        </div><!-- cadre post -->
                        
                        
                        <div class="cadre-post-doc col-xs-12 col-sm-12 col-md-12">
                        
                        <div class="doc-icon"><img src="<?php echo theme_root()?>/img/doc-cr.svg" alt="Compte rendus" data-no-retina /></div>
                        <div class="doc-title">Compte rendus de réunion</div>
                        
                        <div class="doc-ligne separation-gris-clair">
                        <div class="lien-fichier"><a href="#">Compte rendu de réunion du 17 juin</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">108ko</div>
                        </div>
                        <div class="doc-ligne">
                        <div class="lien-fichier"><a href="#">Compte rendu de réunion du 12 avril</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">108ko</div>
                        </div>

                        </div><!-- cadre post -->
                        
                        
                        <div class="cadre-post-doc col-xs-12 col-sm-12 col-md-12">
                        
                        <div class="doc-icon"><img src="<?php echo theme_root()?>/img/doc-autres.svg" alt="Autres documents" data-no-retina /></div>
                        <div class="doc-title">Autres documents</div>
                        
                        <div class="doc-ligne">
                        <div class="lien-fichier"><a href="#">Planning de la semaine 22</a></div><div class="type-fichier">Fichier PDF</div><div class="taille-fichier">237ko</div>
                        </div>

                        </div><!-- cadre post -->


					</main>
                    </div>    						
					
				</div>
                <div class="clearfix"></div>
                <br />
<?php //get_sidebar('right'); ?> 
<?php get_footer(); ?> 