<?php
/**
 * Template for dispalying single post (read full post page).
 * Custom theme made by Gael Ganlut for the web agency R2 Agence Digitale.
 */

get_header();

/* */
?>
<?php get_sidebar('left'); ?>
				<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
					<main id="main" class="site-main">
                    <div class='content-top'></div>
						<?php if (have_posts()) { ?>
						<?php
						// start the loop
						while (have_posts()) {
							echo "<div class='cadre-post col-xs-12 col-sm-12 col-md-12'><div class='profil-post'>"; the_post(); ?>
                            <div class="profil-p">
															<?php $author_id=$post->post_author; $attachment_id = get_user_option( 'metronet_image_id', $author_id ); $urlp = rtrim(wp_get_attachment_url( $attachment_id ), ".jpg");
										if ( $urlp !== '' ) { ?>
                                <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
                                    <defs>
                                        <mask id="svgmask4" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                                        <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m4" />
										<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m4-2x" />
                                        </mask>
                                    </defs>
                                    <image xlink:href="<?php echo $urlp ?>-160x160.jpg" class="attachment-profil-size" mask="url(#svgmask4)" id="p4" y="-10%" x="-10%" width="125%" height="125%" />
									<image xlink:href="<?php echo $urlp ?>-160x160@2x.jpg" class="attachment-profil-size" mask="url(#svgmask4)" id="p4-2x" y="-10%" x="-10%" width="125%" height="125%" />
                            </svg>
														<?php } else { ?>
															<svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
																	<defs>
																			<mask id="svgmask3" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
																			<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m3" />
									<image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m3-2x" />
																			</mask>
																	</defs>
																	<image xlink:href="<?php echo theme_root() ?>/img/profil-defaut.png" class="attachment-profil-size" mask="url(#svgmask3)" data-no-retina y="0%" x="0%" width="100%" height="100%" />
														</svg>
														<?php } ?>
                            </div>
 							<?php  echo "<div class='nomprofil2'>".get_the_author_meta('user_firstname', $author_id)." ".get_the_author_meta('user_lastname', $author_id)."</div>";
							echo "<div class='statut-profil'>".get_the_author_meta('nickname', $author_id)."</div>";
							$time = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ); $the_categories = get_the_category();
							echo "<div class='date-post hidden-xs'>Il y a ";
							echo $time.get_cat_icon($the_categories)."</div>";
							echo "<br /><div class='post-title'><h2><a href='";
							echo the_permalink()."' rel='bookmark' title='Permanent Link to ";
							echo the_title_attribute()."'>";
							echo the_title();
							echo "</a></h2></div>";
							echo "<div class='excerpt'>";
							the_content();
							echo "</div><div class='clearfix'></div>";
							echo "<div class='date-post visible-xs'><br /><br />Il y a ";
							echo $time.get_cat_icon($the_categories)."</div>";
						echo "</div></div>";
						}// end while ?>
                        <?php } else { ?>
						<?php get_template_part('no-results', 'index'); ?>
						<?php }// endif;
						echo "<div class='clearfix'></div><br /><br />";
						// If comments are open or we have at least one comment, load up the comment template
						//	if (comments_open() || '0' != get_comments_number()) {
						//		comments_template();
						//	}
						//echo "\n\n";
						 ?>
					</main>
				</div>
<?php get_sidebar('left'); ?>
<?php get_footer(); ?>
