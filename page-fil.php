<?php
/**
/* Template Name: Fil
 * R2 Agence Digitale.
 */
get_header();
?> 
<?php get_sidebar('left'); ?> 
				<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
					<main id="main" class="site-main">
                    <div class='content-top'></div>
                    <div class='cadre-top-title'>
                    </div>
					
					<div class='cadre-post col-xs-12 col-sm-12 col-md-12 statut-container'>
						<div class="row">
							<input type="text" class="form-control" placeholder="Titre" autofocus>
						</div>
						<div class="row statut-images-container">
							<?php $i = 1;
		                    while($i < 3){ ?>
		                    <div class="post-statut-img">
		                    	
		                        <img class="img-responsive" src="http://lorempicsum.com/up/200/200/<?php echo $i; ?>" alt="">
		                    </div>

		                    <?php $i++; } ?>
		                    <a class="post-statut-img-add">
		                    	<span class="icon statut-img-add-icon"></span>
								Ajouter des photos
		                    </a>
						</div>
						<div class="row">
							<div class="pull-right">
								<label for="statut-permissions" class="statut-label">Qui verra ce statut ?</label>
								 <select class="selectpicker" name="statut-permissions" id="statut-permissions" multiple multiple data-selected-text-format="count>2">
								      <option>Tout le monde</option>
								      <option>Parents uniquement</option>
								      <option>Autre exemple de droit</option>
								  </select>
								<a href="#" class="button-md red">Poster</a>
							</div>	
						</div>
					</div>


						<?php if (have_posts()) { ?> 
						<?php 
						// start the loop
						while (have_posts()) {
							echo "<div class='cadre-post col-xs-12 col-sm-12 col-md-12'><div class='profil-post'>"; the_post(); ?>
 							<?php echo "<div class='excerpt'>";
							the_content();
							echo "</div></div></div>";
						}// end while
						customBasicPagination();
						?> 
						<?php } else { ?> 
						<?php get_template_part('no-results', 'index'); ?>
						<?php }// endif; ?> 
					</main>
				</div>
<?php //get_sidebar('right'); ?> 
			</div>  
             <!--site-content-->
<?php get_footer(); ?> 